// 存放公共js
export const pagination = {
  page: {
    sizes: [10, 20, 50, 100],
    total: null
  },
  pageQuery: {
    pageNum: 1,
    pageSize: 10
  }
}
