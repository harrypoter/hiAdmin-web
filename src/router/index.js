import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'/'el-icon-x' the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path(.*)',
        component: () => import('@/views/redirect/index')
      }
    ]
  },
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [{
      path: 'dashboard',
      name: '首页',
      component: () => import('@/views/dashboard/index'),
      meta: { title: '首页', icon: 'dashboard' }
    }]
  },
  
  {
    path: '/personalCenter',
    component: Layout,
    redirect: '/personalCenter/info',
    children: [{
      path: 'info',
      name: 'personalCenter',
      component: () => import('@/views/personalCenter'),
      meta: { title: '个人信息', icon: '' }
    }],
    hidden: true
  }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router

//异步挂载的路由
//动态需要根据权限加载的路由表
export const asyncRoutes = [
  {
    path: '/system',
    component: Layout,
    redirect: '/dashboard',
    name: 'System',
    meta: { title: '系统管理', icon: 'table', roles: ['/system'] },
    alwaysShow: true, // 只有一个子菜单时也要显示
    children: [
      {
        path: 'user',
        name: 'user',
        component: () => import('@/views/system/user'),
        meta: { title: '用户管理', icon: 'table', roles: ['/system/user']  }
      },
      {
        path: 'role',
        name: 'role',
        component: () => import('@/views/system/role'),
        meta: { title: '角色管理', icon: 'table', roles: ['/system/role'] }
      },
      {
        path: 'menu',
        name: 'menu',
        component: () => import('@/views/system/menu'),
        meta: { title: '菜单管理', icon: 'table', roles: ['/system/menu'] }
      },
      {
        path: 'log',
        name: 'log',
        component: () => import('@/views/system/log'),
        meta: { title: '日志查询', icon: 'table', roles: ['/system/log'] }
      },
      {
        path: 'param',
        name: 'param',
        component: () => import('@/views/system/param'),
        meta: { title: '参数管理', icon: 'table', roles: ['/system/param'] }
      },
      {
        path: 'dict',
        name: 'dict',
        component: () => import('@/views/system/dict'),
        meta: { title: '字典管理', icon: 'table', roles: ['/system/dict'] }
      },
      {
        path: 'dayVisit',
        name: 'dayVisit',
        component: () => import('@/views/system/dayVisit'),
        meta: { title: '访问查询', icon: 'table', roles: ['/system/dayVisit'] }
      },
      {
        path: 'monitor',
        name: 'monitor',
        component: () => import('@/views/system/monitor'),
        meta: { title: '系统监控', icon: 'table', roles: ['/system/monitor'] }
      }
    ]
  },
  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]
