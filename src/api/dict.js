import request from '@/utils/request'

export function dictFind(data) {
  return request({
    url: 'dict/find',
    method: 'post',
    data
  })
}

export function findAllFirstLevelDict() {
  return request({
    url: 'dict/findAllFirstLevelDict',
    method: 'get'
  })
}

export function dictSave(data) {
  return request({
    url: 'dict/save',
    method: 'post',
    data
  })
}
