import { asyncRoutes, constantRoutes } from '@/router'

/**
 * Use meta.role to determine if the current user has permission
 * @param roles
 * @param route
 */
function hasPermission (roles, route) {
  if (route.meta && route.meta.roles) {
    return roles.some(role => route.meta.roles.includes(role))
  } else {
    return true
  }
}

// 递归。。。
export function filterAsyncRoutes (routes, menuPaths) {
  const res = []

  routes.forEach(route => {
    const tmp = { ...route }
    // if (menuPaths.includes(route.path)) {
    if (hasPermission(menuPaths, tmp)) {
      if (tmp.children) {
        tmp.children = filterAsyncRoutes(tmp.children, menuPaths)
      }
      res.push(tmp)
    }
  })
  // console.log('666')
  // console.log(routes)
  // console.log(res)
  return res
}

const state = {
  routes: [],
  addRoutes: []
}

const mutations = {
  SET_ROUTES: (state, routes) => {
    state.addRoutes = routes
    state.routes = constantRoutes.concat(routes)
  }
}

const actions = {
  generateRoutes ({ commit }, menuPaths) {
    return new Promise(resolve => {
      let accessedRoutes
      // console.log(menuPaths)
      accessedRoutes = filterAsyncRoutes(asyncRoutes, menuPaths)
      commit('SET_ROUTES', accessedRoutes)
      resolve(accessedRoutes)
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
