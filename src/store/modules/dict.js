import { dictFind,findAllFirstLevelDict,dictSave } from '@/api/dict'


const state = {
}

const mutations = {
}

const actions = {

  dictFind({ commit },form) {
    return new Promise((resolve, reject) => {
      dictFind(form).then(response => {
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  },
  findAllFirstLevelDict({ commit }) {
    return new Promise((resolve, reject) => {
      findAllFirstLevelDict().then((response) => {
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  },
  dictSave({ commit },form) {
    return new Promise((resolve, reject) => {
      dictSave(form).then(response => {
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  },
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
