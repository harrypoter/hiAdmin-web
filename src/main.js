import Vue from 'vue'

import 'normalize.css/normalize.css' // A modern alternative to CSS resets

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import locale from 'element-ui/lib/locale/lang/en' // lang i18n

import '@/styles/index.scss' // global css

import App from './App'
import store from './store'
import router from './router'

import '@/icons' // icon
import '@/permission' // permission control

import { getToken } from '@/utils/auth'
import VueResource from 'vue-resource' //ajax请求


Vue.use(VueResource)

// 在拦截器中设置vue-resouce请求头
Vue.http.interceptors.push((request, next) => {
  request.headers.set('Authorization', getToken()) //setting request.headers
  next((response) => {
    return response
  })
})

import { pagination } from './common.js'  // 公共js
Vue.prototype.common = pagination

import moment from 'moment' //时间格式化插件
//定义一个全局过滤器实现日期格式化
Vue.filter('datefmt', function (input, fmtString) {
  // 使用momentjs这个日期格式化类库实现日期的格式化功能
  if (input === null || fmtString === null
    || input === '' || fmtString === '') {
    return ''
  }
  return moment(input).format(fmtString)
})

/**
 * If you don't want to use mock-server
 * you want to use MockJs for mock api
 * you can execute: mockXHR()
 *
 * Currently MockJs will be used in the production environment,
 * please remove it before going online ! ! !
 */
if (process.env.NODE_ENV === 'production') {
  const { mockXHR } = require('../mock')
  mockXHR()
}

// set ElementUI lang to EN
// Vue.use(ElementUI, { locale })
// 如果想要中文版 element-ui，按如下方式声明
Vue.use(ElementUI)

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
