# hiAdmin-web

## 介绍
后台管理-页面（脚手架）。Java web项目，后台管理的部分，有很多重复的功能，为了减少重复搭建基础框架和
编写重复代码，抽象出共有的部分形成本vue项目。作为后台管理页面的脚手架。本后台管理采用前后端分离的模式，对
应的后端项目是：<https://gitee.com/harrypoter/hiAdmin>

## 系统架构
1.  基于vue-admin-template。
    <https://github.com/PanJiaChen/vue-admin-template.git>
    


## 部分截图
1.  ![总体](https://gitee.com/pamaomaoa/giteePicture/raw/master/hiAdmin/%E6%80%BB%E4%BD%93.png)
2.  ![用户管理](https://gitee.com/pamaomaoa/giteePicture/raw/master/hiAdmin/%E7%94%A8%E6%88%B7%E7%AE%A1%E7%90%86.png)
3.  ![角色管理](https://gitee.com/pamaomaoa/giteePicture/raw/master/hiAdmin/%E8%A7%92%E8%89%B2%E7%AE%A1%E7%90%86.png)
4.  ![日志查询](https://gitee.com/pamaomaoa/giteePicture/raw/master/hiAdmin/%E6%97%A5%E5%BF%97%E6%9F%A5%E8%AF%A2.png)
5.  ![访问查询](https://gitee.com/pamaomaoa/giteePicture/raw/master/hiAdmin/%E8%AE%BF%E9%97%AE%E6%9F%A5%E8%AF%A2.png)
6.  ![系统监控](https://gitee.com/pamaomaoa/giteePicture/raw/master/hiAdmin/%E7%B3%BB%E7%BB%9F%E7%9B%91%E6%8E%A7.png)

## 采用框架
1.  vue
2.  element-ui
3.  vue-router
4.  vue-resource



#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx



